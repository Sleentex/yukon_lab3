package payroll;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
class Employee {

    private @Id @GeneratedValue Long id;
    private String fName;
    private String lName;
    private String role;

    Employee() {}

    Employee(String name, String role) {
        String splitName[] = name.split(" ");
        this.role = role;
        fName = splitName[0];
        lName = splitName[1];
    }
@JsonIgnore
    public String getName() {
        return this.fName + " " + this.lName;
    }

    public void setName(String name) {
        String[] parts =name.split(" ");
        this.fName = parts[0];
        this.lName = parts[1];
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", fName='" + fName + '\'' +
                ", lName='" + lName + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}